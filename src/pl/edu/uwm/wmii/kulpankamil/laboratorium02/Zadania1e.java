package pl.edu.uwm.wmii.kulpankamil.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadania1e {

    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] tablica = new int[n];
        for (int i = 0; i < n; i++)
            tablica[i] = random.nextInt() % 1000;

        System.out.println(Arrays.toString(tablica));

        int dlugoscCiaguDodatniego = 0;
        int maxDlugoscCiaguDodatniego = 0;

        for(int liczba : tablica){

            if(liczba > 0){
                dlugoscCiaguDodatniego++;
            }
            if(maxDlugoscCiaguDodatniego < dlugoscCiaguDodatniego)
                maxDlugoscCiaguDodatniego = dlugoscCiaguDodatniego;
            if(liczba <= 0){
                dlugoscCiaguDodatniego = 0;
            }

        }
        System.out.println("Dlugosc ciagu najdluzszego ciagu dodatniego: " + maxDlugoscCiaguDodatniego);

    }

}
