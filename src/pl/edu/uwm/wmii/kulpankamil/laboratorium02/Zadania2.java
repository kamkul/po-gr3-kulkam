package pl.edu.uwm.wmii.kulpankamil.laboratorium02;

import java.util.Arrays;
import java.util.Random;

public class Zadania2 {

    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc) {
        Random random = new Random();
        for (int i = 0; i < n; i++)
            tab[i] = random.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
    }

    public static int ileParzystych(int tab[]) {
        int parzyste = 0;
        for (int liczba : tab)
            if (liczba % 2 == 0)
                parzyste++;
        return parzyste;
    }

    public static int ileNieparzystych(int tab[]) {
        int nieparzyste = 0;
        for (int liczba : tab)
            if (liczba % 2 == 0)
                nieparzyste++;
        return nieparzyste;
    }

    public static int ileDodatnich(int tab[]) {
        int dodatnie = 0;
        for (int liczba : tab)
            if (liczba > 0)
                dodatnie++;
        return dodatnie;
    }

    public static int ileUjemnych(int tab[]) {
        int ujemne = 0;
        for (int liczba : tab)
            if (liczba < 0)
                ujemne++;
        return ujemne;
    }

    public static int ileZerowych(int tab[]) {
        int zerowe = 0;
        for (int liczba : tab)
            if (liczba == 0)
                zerowe++;
        return zerowe;
    }

    public static int ileMaksymalnych(int tab[]) {
        int najwieksza = Integer.MIN_VALUE;
        int ileNajwiekszych = 0;
        for (int liczba : tab)
            if (liczba > najwieksza) {
                najwieksza = liczba;
                ileNajwiekszych = 1;
            } else if (liczba == najwieksza)
                ileNajwiekszych++;
        return ileNajwiekszych;
    }

    public static int sumaDodatnich(int tab[]) {
        int sumaDodatnie = 0;
        for (int liczba : tab)
            if (liczba > 0)
                sumaDodatnie += liczba;
        return sumaDodatnie;
    }

    public static int sumaUjemnych(int tab[]) {
        int sumaUjemne = 0;
        for (int liczba : tab)
            if (liczba < 0)
                sumaUjemne += liczba;
        return sumaUjemne;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]) {

        int dlugoscCiaguDodatniego = 0;
        int maxDlugoscCiaguDodatniego = 0;

        for (int liczba : tab) {
            if (liczba > 0) {
                dlugoscCiaguDodatniego++;
            }
            if (maxDlugoscCiaguDodatniego < dlugoscCiaguDodatniego)
                maxDlugoscCiaguDodatniego = dlugoscCiaguDodatniego;
            if (liczba <= 0) {
                dlugoscCiaguDodatniego = 0;
            }
        }
        return maxDlugoscCiaguDodatniego;
    }

    public static void signum(int tab[]){
        for (int i = 0; i < tab.length; i++)
            if (tab[i] > 0)
                tab[i] = 1;
            else if (tab[i] < 0)
                tab[i] = -1;
    }

    public static void odwrocFragment(int tab[], int lewy, int prawy){
        int temp;
        for (int i = lewy - 1; i <= (prawy - lewy + 1) / 2; i++) {
            temp = tab[i];
            tab[i] = tab[prawy - i];
            tab[prawy - i] = temp;
        }
    }

    public static void main(String[] args) {

        int[] tablica = new int[10];
        generuj(tablica, 10, -999, 999);

        System.out.println(Arrays.toString(tablica));
        System.out.println("Ilość parzystych: " + ileParzystych(tablica));
        System.out.println("Ilość nieparzystych: " + ileNieparzystych(tablica));
        System.out.println("Ilość dodatnich: " + ileDodatnich(tablica));
        System.out.println("Ilość ujemnych: " + ileUjemnych(tablica));
        System.out.println("Ilość zerowych: " + ileZerowych(tablica));
        System.out.println("Ilość maksymalnych: " + ileMaksymalnych(tablica));
        System.out.println("Suma dodatnich: " + sumaDodatnich(tablica));
        System.out.println("Suma ujemnych: " + sumaUjemnych(tablica));
        System.out.println("Dlugosc maksymalnego ciagu dodatnich: " + dlugoscMaksymalnegoCiaguDodatnich(tablica));
        System.out.println(Arrays.toString(tablica));
        signum(tablica);
        System.out.println(Arrays.toString(tablica));
        odwrocFragment(tablica, 1, 4);
        System.out.println(Arrays.toString(tablica));

    }
}
