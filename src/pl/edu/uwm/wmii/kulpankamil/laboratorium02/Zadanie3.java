package pl.edu.uwm.wmii.kulpankamil.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadanie3 {

    public static int[][] generuj(int x, int y, int minWartosc, int maxWartosc) {

        Random random = new Random();

        int tablica[][] = new int[x][y];
        for (int i = 0; i < x; i++)
            for (int j = 0; j < y; j++)
                tablica[i][j] = random.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        return tablica;
    }

    public static int[][] iloczynMacierzy(int tab1[][], int tab2[][]) {

        int tab3[][] = new int[tab1.length][tab2.length];
        int liczba;
        for (int i = 0; i < tab1.length; i++)
            for (int j = 0; j < tab2.length; j++) {
                liczba = tab1[i][j] + tab2[j][i];
                tab3[i][j] = liczba;
            }

        return tab3;

    }

    public static void wypisz(int tab[][]){

        for (int[] aTab : tab)
            System.out.println(Arrays.toString(aTab));

    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int m;
        int n;
        int k;
        do
            m = scanner.nextInt();
        while (m > 10 || m < 0);
        do
            n = scanner.nextInt();
        while (n > 10 || n < 0);
        do
            k = scanner.nextInt();
        while (k > 10 || k < 0);


        int[][] macierzA = generuj(m, n, -999, 999);
        int[][] macierzB = generuj(n, k, -999, 999);

        wypisz(macierzA);
        System.out.println();
        wypisz(macierzB);
        System.out.println();

        wypisz(iloczynMacierzy(macierzA,macierzB));
    }
}
