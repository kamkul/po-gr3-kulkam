package pl.edu.uwm.wmii.kulpankamil.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadania1g {

    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] tablica = new int[n];
        for (int i = 0; i < n; i++)
            tablica[i] = random.nextInt() % 1000;

        int lewy = scanner.nextInt();
        int prawy = scanner.nextInt();
        int temp;

        System.out.println(Arrays.toString(tablica));

        for (int i = lewy - 1; i <= (prawy - lewy + 1) / 2; i++) {
            temp = tablica[i];
            tablica[i] = tablica[prawy - i];
            tablica[prawy - i] = temp;
        }


        System.out.println(Arrays.toString(tablica));

    }

}
