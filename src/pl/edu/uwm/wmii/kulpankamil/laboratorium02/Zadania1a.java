package pl.edu.uwm.wmii.kulpankamil.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadania1a {

    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] tablica = new int[n];
        for(int i = 0; i < n; i++)
            tablica[i] = random.nextInt() % 1000;

        System.out.println(Arrays.toString(tablica));

        int nieparzyste = 0;
        int parzyste = 0;
        for(int liczba : tablica)
            if(liczba % 2 == 0)
                parzyste++;
            else
                nieparzyste++;

        System.out.println("Ilość parzystych: " + parzyste);
        System.out.println("Ilość nieparzystych: " + nieparzyste);

    }

}
