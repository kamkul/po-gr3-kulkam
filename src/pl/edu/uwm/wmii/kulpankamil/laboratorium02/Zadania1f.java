package pl.edu.uwm.wmii.kulpankamil.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadania1f {

    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] tablica = new int[n];
        for (int i = 0; i < n; i++)
            tablica[i] = random.nextInt() % 1000;

        System.out.println(Arrays.toString(tablica));

        for (int i = 0; i < n; i++)
            if (tablica[i] > 0)
                tablica[i] = 1;
            else if (tablica[i] < 0)
                tablica[i] = -1;

        System.out.println(Arrays.toString(tablica));

    }

}
