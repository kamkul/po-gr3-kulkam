package pl.edu.uwm.wmii.kulpankamil.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadania1c {

    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] tablica = new int[n];
        for (int i = 0; i < n; i++)
            tablica[i] = random.nextInt() % 5;

        System.out.println(Arrays.toString(tablica));

        int najwieksza = Integer.MIN_VALUE;
        int ileNajwiekszych = 0;
        for (int liczba : tablica)
            if (liczba > najwieksza) {
                najwieksza = liczba;
                ileNajwiekszych = 1;
            }
            else if(liczba == najwieksza)
                ileNajwiekszych++;

        System.out.println("Najwiekszy: " + najwieksza);
        System.out.println("Ilość najwiekszych: " + ileNajwiekszych);


    }

}
