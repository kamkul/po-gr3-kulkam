package pl.edu.uwm.wmii.kulpankamil.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadania1d {

    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] tablica = new int[n];
        for (int i = 0; i < n; i++)
            tablica[i] = random.nextInt() % 5;

        System.out.println(Arrays.toString(tablica));

        int sumaUjemne = 0;
        int sumaDodatnie = 0;
        for(int liczba : tablica)
            if(liczba > 0)
                sumaDodatnie+=liczba;
            else if(liczba < 0)
                sumaUjemne+=liczba;

        System.out.println("Suma dodatnich: " + sumaDodatnie);
        System.out.println("Suma ujemmne: " + sumaUjemne);

    }

}
