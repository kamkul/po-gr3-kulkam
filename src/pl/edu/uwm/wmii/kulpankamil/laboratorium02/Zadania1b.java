package pl.edu.uwm.wmii.kulpankamil.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadania1b {

    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] tablica = new int[n];
        for(int i = 0; i < n; i++)
            tablica[i] = random.nextInt() % 1000;

        System.out.println(Arrays.toString(tablica));

        int ujemne = 0;
        int dodatnie = 0;
        int zerowe = 0;
        for(int liczba : tablica)
            if(liczba > 0)
                dodatnie++;
            else if(liczba < 0)
                ujemne++;
            else
                zerowe++;

        System.out.println("Ilość dodatnich: " + dodatnie);
        System.out.println("Ilość ujemmne: " + ujemne);
        System.out.println("Ilość zerowe: " + zerowe);

    }

}
