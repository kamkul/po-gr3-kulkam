package pl.edu.uwm.wmii.kulpankamil.laboratorium04;

import java.util.ArrayList;
import java.util.Collections;

public class Zadanie1 {

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {

        ArrayList<Integer> list = new ArrayList<>(a);
        list.addAll(b);
        return list;
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {

        ArrayList<Integer> list = new ArrayList<>();

        int wielkosc = a.size() + b.size() - 2;
        for (int i = 0; i < wielkosc; i++) {
            if (i < a.size())
                list.add(a.get(i));
            if (i < b.size())
                list.add(b.get(i));
        }
        return list;
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {

        ArrayList<Integer> list = new ArrayList<>();
        list.addAll(a);
        list.addAll(b);
        Collections.sort(list);

        return list;
    }

//    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
//
//        ArrayList<Integer> list = new ArrayList<>();
//        for (int i = a.size() - 1; i >= 0; i--)
//            list.add(a.get(i));
//
//        return list;
//    }

    public static void reversed(ArrayList<Integer> a) {

        int temp;
        for (int i = 0; i < a.size() / 2; i++) {
            temp = a.get(i);
            a.set(i, a.get(a.size() - 1 - i));
            a.set(a.size() - 1, temp);
        }
    }

    public static void main(String[] args) {

        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        ArrayList<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(2);
        list2.add(3);

        ArrayList<Integer> testAppend = new ArrayList<>();
        ArrayList<Integer> testMerge = new ArrayList<>();
        ArrayList<Integer> testReversed = new ArrayList<>();
        ArrayList<Integer> testMergeSorted = new ArrayList<>();


        // initialise test append list
        testAppend.add(1);
        testAppend.add(2);
        testAppend.add(3);
        testAppend.add(1);
        testAppend.add(2);
        testAppend.add(3);

        // initialise test merge list
        testMerge.add(1);
        testMerge.add(1);
        testMerge.add(2);
        testMerge.add(2);
        testMerge.add(3);
        testMerge.add(3);

        // initialise test reversed list
        testReversed.add(3);
        testReversed.add(2);
        testReversed.add(1);

        // initialise test merge sorted list
        testMergeSorted.add(1);
        testMergeSorted.add(1);
        testMergeSorted.add(2);
        testMergeSorted.add(2);
        testMergeSorted.add(3);
        testMergeSorted.add(3);

        System.out.println(list1);
        System.out.println(list2);

        System.out.println(mergeSorted(list1, list2));
        if (mergeSorted(list1, list2).equals(testMergeSorted))
            System.out.println("Merge sort ok!");
        else
            System.out.println("Merge sort not ok!");

        System.out.println(merge(list1, list2));
        if (merge(list1, list2).equals(testMerge))
            System.out.println("Merge ok!");
        else
            System.out.println("Merge not ok!");

        System.out.println(append(list1, list2));
        if (append(list1, list2).equals(testAppend))
            System.out.println("Append ok!");
        else
            System.out.println("Append not ok!");

        reversed(list1);
        if (list1.equals(testReversed))
            System.out.println("Reverse ok!");
        else
            System.out.println("Reverse not ok!");
        System.out.println(list1);

    }
}
