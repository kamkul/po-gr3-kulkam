package pl.edu.uwm.wmii.kulpankamil.laboratorium05;

public class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private double saldo;

    RachunekBankowy(double saldo){
        this.saldo = saldo;
    }

    void obliczMiesieczneOdsetki(){
        saldo += (saldo * rocznaStopaProcentowa) / 12;
    }

    public static void setRocznaStopaProcentowa(double rocznaStopaProcentowa) {
        RachunekBankowy.rocznaStopaProcentowa = rocznaStopaProcentowa;
    }

    public double getSaldo() {
        return saldo;
    }
}
