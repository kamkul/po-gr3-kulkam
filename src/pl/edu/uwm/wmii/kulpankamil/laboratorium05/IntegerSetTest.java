package pl.edu.uwm.wmii.kulpankamil.laboratorium05;

public class IntegerSetTest {
    public static void main(String[] args) {
        IntegerSet integerSet1 = new IntegerSet();
        integerSet1.insertElement(2);
        integerSet1.insertElement(5);
        integerSet1.insertElement(50);
        integerSet1.insertElement(20);
        integerSet1.insertElement(8);
        integerSet1.insertElement(6);
        integerSet1.insertElement(7);
        integerSet1.deleteElement(7);
        integerSet1.deleteElement(6);

        IntegerSet integerSet2 = new IntegerSet();
        integerSet2.insertElement(2, 5, 50, 20, 8, 6, 7);

        IntegerSet integerSet3 = new IntegerSet();
        integerSet3.insertElement(2);
        integerSet3.insertElement(5);
        integerSet3.insertElement(50);

        System.out.println(integerSet3.toString());

        IntegerSet integerSet4 = new IntegerSet();
        integerSet4.insertElement(9);
        integerSet4.insertElement(5);
        integerSet4.insertElement(41);

        System.out.println(integerSet4.toString());

        System.out.println("Union: " + IntegerSet.union(integerSet3, integerSet4).toString());
        System.out.println("Intersection: " + IntegerSet.intersection(integerSet3, integerSet4).toString());

        System.out.println(integerSet1.toString());
        System.out.println(integerSet1.equals(integerSet2));

    }
}
