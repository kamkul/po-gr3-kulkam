package pl.edu.uwm.wmii.kulpankamil.laboratorium05;

public class RachunekBankowyTest {

    public static void main(String[] args) {

        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);

        System.out.println("Wypisanie pieniedzy saverow przed obliczeniem odsetek");
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());

        RachunekBankowy.rocznaStopaProcentowa = 0.04;
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();

        System.out.println("Wypisanie pieniedzy saverow po obliczeniu odsetek");
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());

        RachunekBankowy.rocznaStopaProcentowa = 0.05;
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();

        System.out.println("Wypisanie pieniedzy saverow po obliczeniu kolejnych odsetek");
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
    }
}
