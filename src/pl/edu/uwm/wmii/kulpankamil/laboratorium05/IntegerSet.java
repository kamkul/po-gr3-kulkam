package pl.edu.uwm.wmii.kulpankamil.laboratorium05;

public class IntegerSet {
    private boolean[] array;

    IntegerSet() {
        this.array = new boolean[100];
    }

    static IntegerSet union(IntegerSet integerSet1, IntegerSet integerSet2) {
        IntegerSet output = new IntegerSet();
        for (int i = 0; i < 100; i++)
            if (integerSet1.getArray()[i] || integerSet2.getArray()[i])
                output.insertElement(i + 1);
        return output;
    }

    static IntegerSet intersection(IntegerSet integerSet1, IntegerSet integerSet2) {
        IntegerSet output = new IntegerSet();
        for (int i = 0; i < 100; i++) {
            if (integerSet1.getArray()[i] && integerSet2.getArray()[i])
                output.insertElement(i + 1);
        }
        return output;
    }

    public boolean[] getArray() {
        return array;
    }

    void insertElement(int... integer) {
        for(int element : integer)
            this.array[element - 1] = true;
    }

    void deleteElement(int... integer) {
        for(int element : integer)
            this.array[element - 1] = false;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < array.length; i++)
            if (array[i]) {
                stringBuilder.append(i + 1);
                stringBuilder.append(" ");
            }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object obj) {

//        if (obj.getClass() != this.getClass())
//            return false;
        if(!(obj instanceof IntegerSet))
            return false;
        IntegerSet other = (IntegerSet) obj;
        for (int i = 0; i < 100; i++)
            if (this.array[i] != other.array[i])
                return false;
        return true;
    }
}
