package pl.edu.uwm.wmii.kulpankamil.dodatkoweZadanie;

public class Czworokat {

    int a;
    int b;
    int c;
    int d;

    Czworokat(int a, int b, int c, int d){
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public int getA(){
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public int getD() {
        return d;
    }

    public int obwod(){
        return a+b+c+d;
    }

}
