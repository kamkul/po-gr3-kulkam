package pl.edu.uwm.wmii.kulpankamil.dodatkoweZadanie;

public class Prostokat extends Czworokat {

    Prostokat(int a, int b) {
        super(a, b, a, b);
    }

    public int obwod(){
        return 2*a+2*b;
    }

    public int pole(){
        return a*b;
    }

}
