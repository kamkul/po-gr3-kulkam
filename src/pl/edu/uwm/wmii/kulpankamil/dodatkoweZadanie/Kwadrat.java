package pl.edu.uwm.wmii.kulpankamil.dodatkoweZadanie;

public class Kwadrat extends Prostokat{
    Kwadrat(int a) {
        super(a, a);
    }

    public int obwod(){
        return 4*a;
    }

    public int pole(){
        return a^2;
    }

    public double przekatna(){
        return Math.pow(a,0.5);
    }

}
