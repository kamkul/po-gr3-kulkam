package pl.edu.uwm.wmii.kulpankamil.dodatkoweZadanie;

public class Main {
    public static void main(String[] args) {

        Czworokat czworokat = new Czworokat(1,2,3,4);
        Prostokat prostokat = new Prostokat(1,2);
        Kwadrat kwadrat = new Kwadrat(1);

        Prostokat bla2 = (Prostokat) new Czworokat(2,4,2,4);

        Czworokat bla3 = new Kwadrat(2);
        Prostokat bla4 = new Kwadrat(2);

        Czworokat bla5 = new Prostokat(2,4);

        System.out.println(prostokat.pole());
        System.out.println(prostokat.obwod());
        System.out.println(kwadrat.obwod());
        System.out.println(kwadrat.pole());
        System.out.println(kwadrat.przekatna());
        System.out.println(czworokat.getA());
    }
}
