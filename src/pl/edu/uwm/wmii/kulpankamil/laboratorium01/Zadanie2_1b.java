package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie2_1b {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int ilosc = 0;

        for (int i = 0; i < n; i++) {
            liczba = scanner.nextInt();
            if (liczba % 3 == 0 && liczba % 5 != 0)
                ilosc++;
        }
        System.out.println("Ilość: " + ilosc);
    }
}
