package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie2_1f {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int ilosc = 0;

        for (int i = 1; i <= n; i++) {
            liczba = scanner.nextInt();
            if (i % 2 != 0 && liczba % 2 == 0)
                ilosc++;
        }
        System.out.println("Ilość: " + ilosc);
    }
}
