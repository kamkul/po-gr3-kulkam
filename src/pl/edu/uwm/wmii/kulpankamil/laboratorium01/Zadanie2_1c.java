package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

import static java.lang.Math.sqrt;

public class Zadanie2_1c {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int ilosc = 0;

        for (int i = 0; i < n; i++) {
            liczba = scanner.nextInt();
            if (sqrt(liczba) % 2 == 0)
                ilosc++;
        }
        System.out.println("Ilość: " + ilosc);
    }
}
