package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie1_1c {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int suma = 0;
        int liczba;

        for (int i = 0; i < n; i++) {
            liczba = scanner.nextInt();
            suma += liczba;
        }

        System.out.println("Suma wpisanych elementów wynosi:" + suma);
    }
}
