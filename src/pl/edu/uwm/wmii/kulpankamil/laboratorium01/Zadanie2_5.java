package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie2_5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int poprzednia;
        if (n > 0) {
            poprzednia = scanner.nextInt();
            for (int i = 1; i <= n; i++) {
                liczba = scanner.nextInt();
                if (poprzednia > 0 && liczba > 0)
                    System.out.println(poprzednia + " " + liczba);
                poprzednia = liczba;
            }
        }
    }
}
