package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

import static java.lang.Math.pow;

public class Zadanie2_1e {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int silnia = 1;
        int ilosc = 0;

        for (int i = 1; i <= n; i++) {
            liczba = scanner.nextInt();
            silnia *= i;
            if (pow(2, i) < liczba && liczba < silnia)
                ilosc++;
        }
        System.out.println("Ilość: " + ilosc);
    }
}
