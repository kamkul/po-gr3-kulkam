package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int pierwsza;
        StringBuilder liczby = new StringBuilder();
        if (n > 0) {
            pierwsza = scanner.nextInt();
            for (int i = 1; i < n; i++) {
                liczba = scanner.nextInt();
                liczby.append(liczba);
                liczby.append(" ");
            }
            liczby.append(pierwsza);
            System.out.println(liczby);
        }
    }
}
