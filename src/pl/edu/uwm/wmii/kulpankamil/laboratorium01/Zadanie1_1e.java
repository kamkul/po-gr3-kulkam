package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

import static java.lang.Math.abs;

public class Zadanie1_1e {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int iloczyn = 1;
        int liczba;

        for (int i = 0; i < n; i++) {
            liczba = scanner.nextInt();
            iloczyn *= abs(liczba);
        }

        System.out.println("Iloczyn wpisanych elementów wynosi:" + iloczyn);
    }
}
