package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie2_2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int suma = 0;

        for (int i = 1; i <= n; i++) {
            liczba = scanner.nextInt();
            if (liczba > 0)
                suma += liczba;
        }

        suma *= 2;
        System.out.println("Podwojona suma wynosi: " + suma);
    }
}
