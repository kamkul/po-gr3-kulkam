package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

import static java.lang.Math.pow;

public class Zadanie1_1f {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int suma = 0;
        int liczba;

        for (int i = 0; i < n; i++) {
            liczba = scanner.nextInt();
            suma += pow(liczba, 2);
        }

        System.out.println("Suma wpisanych elementów wynosi:" + suma);
    }
}
