package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

import static java.lang.Math.pow;

public class Zadanie1_1i {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double suma = 0;
        int silnia = 1;
        int liczba;

        for (int i = 1; i <= n; i++) {
            liczba = scanner.nextInt();
            silnia *= i;
            suma += pow(-1, i) * liczba / silnia;
        }

        System.out.println("Suma wpisanych elementów wynosi:" + suma);
    }
}
