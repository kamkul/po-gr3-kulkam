package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie2_3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int dodatnie = 0;
        int zerowe = 0;
        int ujemne = 0;

        for (int i = 1; i <= n; i++) {
            liczba = scanner.nextInt();
            if (liczba > 0)
                dodatnie++;
            else if (liczba < 0)
                ujemne++;
            else
                zerowe++;
        }

        System.out.println("Ilość dodatnich: " + dodatnie);
        System.out.println("Ilość ujemmne: " + ujemne);
        System.out.println("Ilość zerowe: " + zerowe);
    }
}
