package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

import static java.lang.Math.abs;
import static java.lang.Math.pow;

public class Zadanie2_1h {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int ilosc = 0;

        for (int i = 1; i <= n; i++) {
            liczba = scanner.nextInt();
            if(abs(liczba) < pow(i,2))
                ilosc++;
        }
        System.out.println("Ilość: " + ilosc);
    }
}
