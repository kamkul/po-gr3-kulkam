package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class Zadanie1_1d {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double suma = 0;
        int liczba;

        for (int i = 0; i < n; i++) {
            liczba = scanner.nextInt();
            suma += sqrt(abs(liczba));
        }

        System.out.println("Suma wpisanych elementów wynosi:" + suma);
    }
}
