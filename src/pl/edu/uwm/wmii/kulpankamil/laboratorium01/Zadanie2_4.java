package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie2_4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i = 1; i <= n; i++) {
            liczba = scanner.nextInt();
            if (liczba > max)
                max = liczba;
            if (liczba < min)
                min = liczba;
        }

        System.out.println("Minimum: " + min);
        System.out.println("Maximum: " + max);
    }
}
