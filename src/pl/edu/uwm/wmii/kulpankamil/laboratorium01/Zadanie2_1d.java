package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie2_1d {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int a1;
        int a2;
        int ilosc = 0;

        if (n > 2) {
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            for (int i = 3; i <= n; i++) {
                liczba = scanner.nextInt();
                if (a2 < ((a1 + liczba) / 2))
                    ilosc++;
                a1 = a2;
                a2 = liczba;
            }
            System.out.println("Ilość: " + ilosc);
        }
    }
}
