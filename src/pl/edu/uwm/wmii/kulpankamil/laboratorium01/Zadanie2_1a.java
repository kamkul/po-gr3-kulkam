package pl.edu.uwm.wmii.kulpankamil.laboratorium01;

import java.util.Scanner;

public class Zadanie2_1a {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int liczba;
        int iloscNieparzyste = 0;

        for (int i = 0; i < n; i++) {
            liczba = scanner.nextInt();
            if (liczba < 0)
                iloscNieparzyste++;
        }
        System.out.println("Ilość nieparzystych: " + iloscNieparzyste);
    }
}
