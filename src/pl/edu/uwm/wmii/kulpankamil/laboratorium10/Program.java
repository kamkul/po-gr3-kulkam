package pl.edu.uwm.wmii.kulpankamil.laboratorium10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) throws FileNotFoundException {
        for (String filename : args) {
            ArrayList<String> liniePliku = new ArrayList<>();
            File plik = new File(filename);
            Scanner scanner = new Scanner(plik);
            while (scanner.hasNextLine())
                liniePliku.add(scanner.nextLine());
            Collections.sort(liniePliku);

            System.out.println(liniePliku.toString());
        }
    }
}
