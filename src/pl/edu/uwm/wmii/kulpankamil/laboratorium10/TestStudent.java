package pl.edu.uwm.wmii.kulpankamil.laboratorium10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {

    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList<>();
        grupa.add(new Student("Kulpan", LocalDate.of(1998, 5, 30), 4.5));
        grupa.add(new Student("Pilkowski", LocalDate.of(1998, 5, 30), 4.2));
        grupa.add(new Student("Wojeciechowski", LocalDate.of(1997, 6, 29), 4));
        grupa.add(new Student("Kulpan", LocalDate.of(1998, 5, 30), 2.3));
        grupa.add(new Student("Zawal", LocalDate.of(1991, 2, 2), 3.0));

        wyswietl(grupa);
        System.out.println("Tera posortowane");
        Collections.sort(grupa);
        wyswietl(grupa);
//        Student AAAAA = new Student("a", LocalDate.of(1923,3, 23), 4.2);
//        Student BBBBB = (Student) AAAAA.clone();
//        System.out.println(BBBBB.toString());
//        AAAAA.setNazwisko("AAAAA");
//        System.out.println(BBBBB.toString());
    }

    private static void wyswietl(ArrayList<Student> grupa) {
        for (Student student : grupa)
            System.out.println(student.toString());
    }

}
