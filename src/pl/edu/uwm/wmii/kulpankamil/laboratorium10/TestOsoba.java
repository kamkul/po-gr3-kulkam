package pl.edu.uwm.wmii.kulpankamil.laboratorium10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();
        grupa.add(new Osoba("Kulpan", LocalDate.of(1998, 5, 30)));
        grupa.add(new Osoba("Pilkowski", LocalDate.of(1998, 5, 30)));
        grupa.add(new Osoba("Wojeciechowski", LocalDate.of(1997, 6, 29)));
        grupa.add(new Osoba("Kulpan", LocalDate.of(1994, 9, 3)));
        grupa.add(new Osoba("Zawal", LocalDate.of(1991, 2, 2)));

        wyswietl(grupa);
        System.out.println("Tera posortowane");
        Collections.sort(grupa);
        wyswietl(grupa);
    }

    private static void wyswietl(ArrayList<Osoba> grupa) {
        for (Osoba osoba : grupa)
            System.out.println(osoba.toString());
    }

}
