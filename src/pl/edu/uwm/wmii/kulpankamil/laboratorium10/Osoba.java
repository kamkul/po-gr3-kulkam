package pl.edu.uwm.wmii.kulpankamil.laboratorium10;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Osoba implements Comparable<Osoba> {

    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    @Override
    public int compareTo(Osoba o) {
        if (this.nazwisko.compareTo(o.nazwisko) == 0)
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        return this.nazwisko.compareTo(o.nazwisko);
    }

    @Override
    public String toString() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return getClass().getSimpleName() + " [" + nazwisko + " " + dateTimeFormatter.format(dataUrodzenia) + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Osoba) {
            Osoba other = (Osoba) obj;
            return other.getDataUrodzenia().equals(dataUrodzenia) && other.getNazwisko().equals(nazwisko);
        }
        return false;
    }
}
