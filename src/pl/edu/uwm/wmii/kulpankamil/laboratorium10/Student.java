package pl.edu.uwm.wmii.kulpankamil.laboratorium10;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable {

    private double sredniaOcen;

    Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public int compareTo(Osoba o) {
        int tmp = super.compareTo(o);
        if (tmp == 0 && o instanceof Student) {
            Student other = (Student) o;
            return Double.compare(other.sredniaOcen, sredniaOcen);
        }
        return tmp;
    }

    @Override
    public Object clone(){
        try {
//            Student v = (Student)super.clone();
//            v.setNazwisko(super.getNazwisko());
//            v.setDataUrodzenia(super.getDataUrodzenia());
//            v.setSredniaOcen(getSredniaOcen());
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e);
        }
    }

    @Override
    public String toString() {
        return super.toString().substring(0, super.toString().length() - 1) + " " + sredniaOcen + "]";
    }
}
