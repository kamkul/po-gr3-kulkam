package pl.edu.uwm.wmii.kulpankamil.laboratorium13;

import java.util.Scanner;

public class Zadanie3 {

    private void start() {
        Studenci studenci = new Studenci();
        Scanner scanner = new Scanner(System.in);
        String wejscie = "";
        System.out.println("DodajOcene <id> <ocena>");
        System.out.println("DodajStudenta <id> <imie> <nazwisko>");
        System.out.println("UsunOcene <id>");
        System.out.println("UsunStudenta <id>");
        System.out.println("ZmienOcene <id>");
        System.out.println("Wypisz");
        System.out.println("Zakoncz");

        while (!wejscie.toLowerCase().equals("zakoncz")) {
            wejscie = scanner.nextLine();
            String[] splittedWejscie = wejscie.split(" +");
            if (splittedWejscie.length > 3) {
                if (splittedWejscie[0].toLowerCase().equals("dodajstudenta")) {
                    studenci.dodajStudenta(splittedWejscie[1], splittedWejscie[2], splittedWejscie[3]);
                    continue;
                }
            }
            if (splittedWejscie.length > 2) {
                if (splittedWejscie[0].toLowerCase().equals("dodajocene")) {
                    studenci.dodajOcene(splittedWejscie[1], splittedWejscie[2]);
                    continue;
                }
                if (splittedWejscie[0].toLowerCase().equals("zmienocene")) {
                    studenci.zmienOcene(splittedWejscie[1], splittedWejscie[2]);
                    continue;
                }
            }
            if (splittedWejscie.length > 1) {
                if (splittedWejscie[0].toLowerCase().equals("usunstudenta")) {
                    studenci.usunStudenta(splittedWejscie[1]);
                    studenci.usunOcene(splittedWejscie[1]);
                    continue;
                }
                if (splittedWejscie[0].toLowerCase().equals("usunocene")) {
                    studenci.usunOcene(splittedWejscie[1]);
                    continue;
                }
            }
            if (splittedWejscie.length > 0)
                if (wejscie.toLowerCase().equals("wypisz"))
                    studenci.wypisz();
        }
    }

    public static void main(String[] args) {
        Zadanie3 zadanie2 = new Zadanie3();
        zadanie2.start();
    }
}
