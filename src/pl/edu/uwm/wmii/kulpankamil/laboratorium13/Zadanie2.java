package pl.edu.uwm.wmii.kulpankamil.laboratorium13;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Zadanie2 {

    private static boolean sprawdzOcene(String ocena) {
        switch (ocena) {
            case "ndst":
            case "dst-":
            case "dst":
            case "dst+":
            case "db-":
            case "db":
            case "db+":
            case "bdb-":
            case "bdb":
                return true;
        }
        return false;
    }

    private void start() {
        HashMap<String, String> mapaZeStudentami = new HashMap<>();
        Scanner scanner = new Scanner(System.in);
        String wejscie = "";
        System.out.println("Dodaj <nazwisko> <ocena>");
        System.out.println("Usun <nazwisko>");
        System.out.println("Wypisz");
        System.out.println("Zakoncz");

        while (!wejscie.toLowerCase().equals("zakoncz")) {
            wejscie = scanner.nextLine();
            String[] splittedWejscie = wejscie.split(" ");
            if (splittedWejscie.length > 2) {
                if (sprawdzOcene(splittedWejscie[2])) {
                    if (splittedWejscie[0].toLowerCase().equals("dodaj")) {
                        mapaZeStudentami.put(splittedWejscie[1], splittedWejscie[2]);
                        System.out.println("Dodano pomyslnie studenta z ocena");
                        continue;
                    }
                    if (splittedWejscie[0].toLowerCase().equals("zmien")) {
                        if (!mapaZeStudentami.containsKey(splittedWejscie[1])) {
                            System.out.println("Nie ma takiego studenta");
                            continue;
                        }
                        mapaZeStudentami.replace(splittedWejscie[1], splittedWejscie[2]);
                    }
                }
            }
            if (splittedWejscie.length > 1) {
                if (splittedWejscie[0].toLowerCase().equals("usun")) {
                    if (!mapaZeStudentami.containsKey(splittedWejscie[1])) {
                        System.out.println("Nie ma takiego studenta");
                        continue;
                    }
                    mapaZeStudentami.remove(splittedWejscie[1]);
                    System.out.println("Usunieto pomyslnie studenta");
                }
            }
            if (wejscie.equals("wypisz")) {
                Iterator it = mapaZeStudentami.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    System.out.println(entry.getKey() + ": " + entry.getValue());
                    it.remove();
                }
            }
        }
    }


    public static void main(String[] args) {
        Zadanie2 zadanie2 = new Zadanie2();
        zadanie2.start();
    }
}
