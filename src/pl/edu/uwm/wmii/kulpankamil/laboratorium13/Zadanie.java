package pl.edu.uwm.wmii.kulpankamil.laboratorium13;

public class Zadanie implements Comparable<Zadanie> {

    private String opis;
    private int priorytet;

    Zadanie(String opis, int priorytet) {
        this.opis = opis;
    }

    @Override
    public int compareTo(Zadanie o) {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        if(priorytet >= 0 && priorytet < 10)
            this.priorytet = priorytet;
    }

    public int getPriorytet() {
        return priorytet;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getOpis() {
        return opis;
    }

}