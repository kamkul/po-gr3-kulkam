package pl.edu.uwm.wmii.kulpankamil.laboratorium13;

public class Student implements Comparable<Student> {
    private String imie;
    private String nazwisko;

    Student(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    @Override
    public int compareTo(Student o) {
        return 0;
    }

    @Override
    public String toString() {
        return imie + " " + nazwisko + " ";
    }

}
