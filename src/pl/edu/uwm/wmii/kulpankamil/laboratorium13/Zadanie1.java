package pl.edu.uwm.wmii.kulpankamil.laboratorium13;

import java.util.PriorityQueue;
import java.util.Scanner;

public class Zadanie1 {

    private void start() {
        PriorityQueue<Zadanie> kolejka = new PriorityQueue<>();
        Scanner scanner = new Scanner(System.in);
        String input = "";
        String[] splittedInput;
        while (!input.equals("zakoncz")) {
            input = scanner.nextLine();
            splittedInput = input.split(" +");
            switch (splittedInput[0].toLowerCase()) {
                case "dodaj": {
                    if (splittedInput.length > 2)
                        if (Integer.valueOf(splittedInput[1]) >= 0
                                && Integer.valueOf(splittedInput[1]) < 10)
                            kolejka.add(new Zadanie(splittedInput[2], Integer.valueOf(splittedInput[1])));
                    break;
                }
                case "nastepne": {
                    System.out.println(kolejka.poll().getOpis());
                    break;
                }
                default: {
                    System.out.println("LOL, chyba coś nie pykło, spróbuj jeszcze raz");
                }
            }
        }

        kolejka.poll();
    }

    public static void main(String[] args) {
        Zadanie1 zadanie1 = new Zadanie1();
        zadanie1.start();
    }

}
