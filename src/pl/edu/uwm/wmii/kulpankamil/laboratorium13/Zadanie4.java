package pl.edu.uwm.wmii.kulpankamil.laboratorium13;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Zadanie4 {
    public static void main(String[] args) throws FileNotFoundException {

        String filename = "duzyPlik.txt";
        ArrayList<String[]> liniePliku = new ArrayList<>();
        HashMap<Integer, HashSet<String>> mapa = new HashMap<>();
        File plik = new File(filename);
        Scanner scanner = new Scanner(plik);
        while (scanner.hasNextLine()) {
            String liniaPliku = scanner.nextLine();
            liniePliku.add(liniaPliku.split(" +"));
        }

        HashSet<String> tmp;
        for (String[] linia : liniePliku) {
            for (String wyraz : linia) {
                if (!mapa.containsKey(wyraz.hashCode())) {
                    tmp = new HashSet<>();
                    tmp.add(wyraz);
                    mapa.put(wyraz.hashCode(), tmp);
                } else{
                    tmp = mapa.get(wyraz.hashCode());
                    System.out.println(tmp);
                    tmp.add(wyraz);
                    tmp.add(wyraz);
                    mapa.replace(wyraz.hashCode(), tmp);
                }
            }
        }
        System.out.println();
            mapa.forEach((k,v) -> {
                if(v.size() > 0)
                    System.out.println(Arrays.toString(v.toArray()));
            });
    }

}
