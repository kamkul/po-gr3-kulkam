package pl.edu.uwm.wmii.kulpankamil.laboratorium13;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Studenci {

    private HashMap<String, String> studenciOceny;
    private HashMap<String, Student> studenciOsoby;

    Studenci() {
        studenciOsoby = new HashMap<>();
        studenciOceny = new HashMap<>();
    }

    private static boolean sprawdzOcene(String ocena) {
        switch (ocena) {
            case "ndst":
            case "dst-":
            case "dst":
            case "dst+":
            case "db-":
            case "db":
            case "db+":
            case "bdb-":
            case "bdb":
                return true;
        }
        return false;
    }

    void dodajStudenta(String id, String imie, String nazwisko) {
        if (!studenciOsoby.containsKey(id))
            studenciOsoby.put(id, new Student(imie, nazwisko));
    }

    void dodajOcene(String id, String ocena) {
        if (studenciOsoby.containsKey(id))
            if(sprawdzOcene(ocena))
                studenciOceny.put(id, ocena);
    }

    void usunStudenta(String id) {
        studenciOceny.remove(id);
        studenciOsoby.remove(id);
    }

    void usunOcene(String id) {
        studenciOceny.remove(id);
    }

    void zmienOcene(String id, String ocena) {
        if(sprawdzOcene(ocena))
            studenciOceny.replace(id, ocena);
    }

    void wypisz() {
        Iterator it = studenciOceny.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            System.out.println(entry.getKey() + " " + studenciOsoby.get(entry.getKey()).toString() + ": " + entry.getValue());
            it.remove();
        }
    }
}
