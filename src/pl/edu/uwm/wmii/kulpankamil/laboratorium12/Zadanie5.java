package pl.edu.uwm.wmii.kulpankamil.laboratorium12;

import java.util.Arrays;
import java.util.Stack;

public class Zadanie5 {

    static <T> void wypiszStos(Stack<T> stos) {
        if (stos.size() == 0)
            return;
        System.out.print(stos.pop() + " ");
        wypiszStos(stos);
    }

    private static void upiekszWyrazy(String[] wyrazy) {
        for (int i = 0; i < wyrazy.length; i++) {
            if (wyrazy[i].contains(".")) {
                wyrazy[i] = wyrazy[i].substring(0, 1).toUpperCase() + wyrazy[i].substring(1, wyrazy[i].length() - 1);
            } else if (Character.isUpperCase(wyrazy[i].charAt(0))) {
                wyrazy[i] = wyrazy[i].toLowerCase();
                wyrazy[i] += '.';
            }
        }
    }

    public static void main(String[] args) {
        Stack<String> stos = new Stack<>();
        String zdanko = "Ala ma kota. Jej kot lubi myszy.";
        String[] zdania = zdanko.split("\\. |\\.");
        System.out.println(Arrays.toString(zdania));
        String[] wyrazy;
        for (String zdanie : zdania) {
            zdanie += ".";
            wyrazy = zdanie.split(" ");
            upiekszWyrazy(wyrazy);
            stos.addAll(Arrays.asList(wyrazy));
            wypiszStos(stos);
        }
    }
}
