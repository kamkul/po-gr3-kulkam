package pl.edu.uwm.wmii.kulpankamil.laboratorium12;

import java.util.ArrayList;

public class Zadanie7 {

    private static ArrayList<Integer> sitoEratostenesa(int n) {
        ArrayList<Integer> wynik = new ArrayList<>();
        boolean[] pierwsze = new boolean[n - 2];
        for (int i = 0; i < n - 2; i++)
            pierwsze[i] = true;

        for (int p = 2; p * p < pierwsze.length + 2; p++)
            if (pierwsze[p])
                for (int i = p * p; i < pierwsze.length + 2; i += p)
                    pierwsze[i - 2] = false;

        for (int i = 0; i < pierwsze.length; i++)
            if (pierwsze[i])
                wynik.add(i + 2);

        return wynik;
    }

    public static void main(String[] args) {
        for (int i = 3; i < 1000; i++) {
            ArrayList<Integer> a = sitoEratostenesa(i);
            Zadanie8.print(a);
        }
    }
}
