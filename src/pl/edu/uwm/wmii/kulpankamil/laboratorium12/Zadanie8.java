package pl.edu.uwm.wmii.kulpankamil.laboratorium12;

import java.util.ArrayList;
import java.util.LinkedList;

public class Zadanie8 {
    public static <T extends Iterable<E>, E> void print(T iterableObject) {
        StringBuilder sb = new StringBuilder();
        for (E element : iterableObject)
            sb.append(element).append(", ");
        System.out.println(sb.substring(0, sb.length() - 2));
    }

    public static void main(String[] args) {
        ArrayList<String> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        LinkedList<String> c = new LinkedList<>();
        LinkedList<Integer> d = new LinkedList<>();

        a.add("A");
        a.add("B");

        b.add(1);
        b.add(2);

        c.add("A");
        c.add("B");

        d.add(1);
        d.add(2);

        print(a);
        print(b);
        print(c);
        print(d);
    }
}
