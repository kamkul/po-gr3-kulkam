package pl.edu.uwm.wmii.kulpankamil.laboratorium12;

import java.util.LinkedList;

public class Zadanie4 {

    private static <T> void odwroc(LinkedList<T> lista){
        for(int i = 0; i < lista.size() / 2; i++){
            T tmp = lista.get(i);
            lista.set(i, lista.get(lista.size() - 1 - i));
            lista.set(lista.size() - 1 - i, tmp);
        }
    }

    public static void main(String[] args) {
        LinkedList<String> a = new LinkedList<>();
        a.add("1");
        a.add("2");
        a.add("3");
        a.add("4");
        a.add("5");
        a.add("6");
        a.add("7");
        a.add("8");
        a.add("9");
        odwroc(a);
        System.out.println(a.toString());
    }
}

