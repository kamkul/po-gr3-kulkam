package pl.edu.uwm.wmii.kulpankamil.laboratorium12;

import java.util.Stack;

public class Zadanie6 {
    private static Stack<Integer> podzielLiczbeNaCyfry(int liczba){
        Stack<Integer> stos = new Stack<>();
        int dlugoscLiczby = String.valueOf(liczba).length();
        for(int i = 0; i < dlugoscLiczby; i++){
            stos.push(liczba % 10);
            liczba /= 10;
        }
        return stos;
    }

    public static void main(String[] args) {
        Zadanie5.wypiszStos(podzielLiczbeNaCyfry(2015));
    }
}
