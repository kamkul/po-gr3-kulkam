package pl.edu.uwm.wmii.kulpankamil.laboratorium12;

import java.util.LinkedList;

public class Zadanie2 {
    private static <T> void redukuj(LinkedList<T> pracownicy, int n) {
        int licznik = 0;
        for (int i = 0; i < pracownicy.size(); i++) {
            licznik++;
            if (licznik % n == 0) {
                pracownicy.remove(i);
                licznik = 1;
            }
        }
    }

    public static void main(String[] args) {
        LinkedList<Integer> a = new LinkedList<>();
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);
        a.add(6);
        a.add(7);
        a.add(8);
        a.add(9);

        Zadanie2.redukuj(a, 3);
        System.out.println(a);
    }
}
