package pl.edu.uwm.wmii.kulpankamil.laboratorium11;

import java.lang.reflect.Array;

public class ArrayUtil<T extends Comparable<? super T>> {
    static <T extends Comparable<T>>
    boolean isSorted(T[] array) {
        for (int i = 0; i < array.length - 1; i++)
            if (array[i].compareTo(array[i + 1]) > 0)
                return false;
        return true;
    }

    static <T extends Comparable<? super T>>
    int binSearch(T[] array, T key) {
        int low = 0;
        int high = array.length - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            Comparable<? super T> midVal = array[mid];
            int cmp = midVal.compareTo(key);

            if (cmp < 0)
                low = mid + 1;
            else if (cmp > 0)
                high = mid - 1;
            else
                return mid;
        }
        return -(low + 1);
    }

    static <T extends Comparable<T>>
    void selectionSort(T[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int min_index = i;
            for (int j = i + 1; j < array.length; j++)
                if (array[j].compareTo(array[min_index]) < 0)
                    min_index = j;
            T tmp = array[min_index];
            array[min_index] = array[i];
            array[i] = tmp;
        }
    }

    static <T extends Comparable<T>>
    void mergeSort(T[] array) {
        if (array.length > 1) {
            int mid = array.length / 2;

            @SuppressWarnings("unchecked")
            T[] left = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length / 2);
            @SuppressWarnings("unchecked")
            T[] right = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length - mid);

            System.arraycopy(array, 0, left, 0, mid);
            if (array.length - mid >= 0)
                System.arraycopy(array, mid, right, 0, array.length - mid);

            System.arraycopy(array, 0, left, 0, mid);
            System.arraycopy(array, mid, right, 0, array.length - mid);

            mergeSort(left);
            mergeSort(right);

            int i = 0;
            int j = 0;
            int k = 0;

            while (i < left.length && j < right.length) {
                if (left[i].compareTo(right[j]) < 0) {
                    array[k] = left[i];
                    i++;
                } else {
                    array[k] = right[j];
                    j++;
                }
                k++;
            }
            while (i < left.length) {
                array[k] = left[i];
                i++;
                k++;
            }
            while (j < right.length) {
                array[k] = right[j];
                j++;
                k++;
            }
        }
    }
}
