package pl.edu.uwm.wmii.kulpankamil.laboratorium11;

// Words in hash set demo

public class PairUtil {

    static <T> Pair<T> swap1(Pair<T> pair) {

        return new Pair<T>(pair.getSecond(), pair.getFirst());
    }

    static <T> void swap2(Pair<T> pair) {
        T tmp = pair.getFirst();
        pair.setFirst(pair.getSecond());
        pair.setSecond(tmp);
    }

}
