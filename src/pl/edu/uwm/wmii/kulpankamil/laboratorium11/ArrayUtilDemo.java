package pl.edu.uwm.wmii.kulpankamil.laboratorium11;

import pl.edu.uwm.wmii.kulpankamil.laboratorium10.Osoba;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.Arrays;

public class ArrayUtilDemo {
    public static void main(String[] args) {
        Integer[] a = {1, 2, 3, 4, 5};
        LocalDate[] b = {LocalDate.of(1992, 1, 3),
                LocalDate.of(1993, 2, 3),
                LocalDate.of(1994, 2, 4)};

        Integer[] c = {5, 4, 3, 2, 1};
        LocalDate[] d = {LocalDate.of(1994, 1, 3),
                LocalDate.of(1993, 2, 3),
                LocalDate.of(1992, 2, 4)};

        Osoba[] e = {new Osoba("Kulpan", LocalDate.of(1992,3,4)),
                new Osoba("Kulpan", LocalDate.of(1991,3,4))};

        System.out.println(ArrayUtil.isSorted(a));
        System.out.println(ArrayUtil.isSorted(b));
        System.out.println(ArrayUtil.isSorted(e));
        System.out.println(ArrayUtil.isSorted(d));

        System.out.println("BinSearch " + ArrayUtil.binSearch(a, 2));
        System.out.println("BinSearch " + ArrayUtil.binSearch(b, LocalDate.of(1992, 1, 3)));

//        ArrayUtil.selectionSort(c);
//        ArrayUtil.selectionSort(d);

        ArrayUtil.mergeSort(c);
        ArrayUtil.mergeSort(d);

        System.out.println(Arrays.toString(c));
        System.out.println(Arrays.toString(d));

    }
}
