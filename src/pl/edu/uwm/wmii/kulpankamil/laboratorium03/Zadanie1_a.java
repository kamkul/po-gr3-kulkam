package pl.edu.uwm.wmii.kulpankamil.laboratorium03;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Scanner;

public class Zadanie1_a {

    public static int countChar(String str, char c) {

        int ilosc = 0;
        for (int i = 0; i < str.length(); i++)
            if (c == str.charAt(i))
                ilosc++;
        return ilosc;
    }

    public static int countSubStr(String str, String substr) {

        int ilosc = 0;
        for (int i = 0; i < str.length() - substr.length() + 1; i++)
            if (str.substring(i, i + substr.length()).equals(substr))
                ilosc++;
        return ilosc;
    }

    public static String middle(String str) {

        if (str.length() % 2 != 0)
            return String.valueOf(str.charAt(str.length() / 2));
        else
            return str.substring(str.length() / 2 - 1, str.length() / 2 + 1);

    }

    public static String repeat(String str, int n) {

        for (int i = 0; i < n; i++)
            str += str;

        return str;
    }

    public static int[] where(String str, String substr) {

        int tab[] = new int[str.length()];
        int j = 0;
        for (int i = 0; i < str.length() - substr.length() + 1; i++)
            if (str.substring(i, i + substr.length()).equals(substr)) {
                tab[j] = i;
                j++;
            }


        return tab;
    }

    public static String change(String str) {

        StringBuffer stringbuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++)
            if (Character.isUpperCase(str.charAt(i)))
                stringbuffer.append(Character.toLowerCase(str.charAt(i)));
            else if (Character.isLowerCase(str.charAt(i)))
                stringbuffer.append(Character.toUpperCase(str.charAt(i)));

        return stringbuffer.toString();
    }

    public static String nice(String str) {

        StringBuffer stringBuffer = new StringBuffer(str);
        for (int i = stringBuffer.length() - 1; i > 0; i--)
            if (i % 3 == 0)
                stringBuffer.insert(i, ',');

        return stringBuffer.toString();
    }

    public static String nice(String str, int liczbaPozycji, char separator) {

        StringBuffer stringBuffer = new StringBuffer(str);
        for (int i = stringBuffer.length() - 1; i > 0; i--)
            if (i % liczbaPozycji == 0)
                stringBuffer.insert(i, separator);

        return stringBuffer.toString();
    }

    public static int zliczZnaki(String nazwaPliku, char znak) throws FileNotFoundException {

        File plik = new File(nazwaPliku);
        Scanner scanner = new Scanner(plik);
        String linia;
        int iloscZnakow = 0;

        while (scanner.hasNextLine()) {
            linia = scanner.nextLine();
            iloscZnakow += countChar(linia, znak);
        }

        return iloscZnakow;
    }

    public static int zliczWyrazy(String nazwaPliku, String wyraz) throws FileNotFoundException {

        File plik = new File(nazwaPliku);
        Scanner scanner = new Scanner(plik);
        String linia;
        int iloscZnakow = 0;

        while (scanner.hasNextLine()) {
            linia = scanner.nextLine();
            iloscZnakow += countSubStr(linia, wyraz);
        }

        return iloscZnakow;
    }

    public static BigInteger ziarnko(int n) {

        BigInteger wynik = BigInteger.valueOf(1);
        for (int i = 0; i < n * n; i++) {

            wynik = wynik.multiply(BigInteger.valueOf(2));

        }
        return wynik;
    }

    public static BigDecimal lokata(double k, double p, double n) {

        BigDecimal wynik = BigDecimal.valueOf(k);
        for (int i = 0; i < n; i++)
            wynik = wynik.multiply(BigDecimal.valueOf(p));

        return wynik;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        System.out.println(middle(string));
        System.out.println(change(string));
        System.out.println(nice(string));
        System.out.println(zliczZnaki("plik.txt", 'a'));
        System.out.println(zliczWyrazy("plik.txt", "Labrador"));
        System.out.println("Ziarnko dla 8x8: " + ziarnko(8));
        System.out.println("Lokata dla 8,8,8: " + lokata(8, 8, 8));
    }
}
