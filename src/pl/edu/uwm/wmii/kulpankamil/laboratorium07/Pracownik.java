package pl.edu.uwm.wmii.kulpankamil.laboratorium07;

import java.time.LocalDate;

class Pracownik extends Osoba {
    public Pracownik(String[] imiona, String nazwisko, LocalDate dataUrodzenia, boolean plec, double pobory,
                     LocalDate dataZatrudnienia) {
        super(imiona, nazwisko, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }

    public String getOpis() {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    private double pobory;

    private LocalDate dataZatrudnienia;
}

