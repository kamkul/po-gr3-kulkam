package pl.edu.uwm.wmii.kulpankamil.laboratorium07;

import java.time.LocalDate;

abstract class Osoba {
    public Osoba(String[] imiona, String nazwisko, LocalDate dataUrodzenia, boolean plec) {
        this.imiona = imiona;
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    String[] imiona;

    LocalDate dataUrodzenia;

    boolean plec;

    public abstract String getOpis();

    public String getNazwisko() {
        return nazwisko;
    }

    public String[] getImiona() {
        return imiona;
    }

    public String getImionaAsString(){
        StringBuilder output = new StringBuilder();
        for(String imie : imiona)
            output.append(imie).append(" ");
        return output.toString().substring(0, output.toString().length()-1);
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    private String nazwisko;
}

