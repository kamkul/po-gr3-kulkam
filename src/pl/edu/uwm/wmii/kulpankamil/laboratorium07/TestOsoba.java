package pl.edu.uwm.wmii.kulpankamil.laboratorium07;

import java.time.LocalDate;

public class TestOsoba {
    public static void main(String[] args) {
        Osoba[] ludzie = new Osoba[2];


        ludzie[0] = new Pracownik(new String[]{"Jan", "Andrzej"}, "Wyzdrzej",
                LocalDate.of(1998, 10, 12), true, 3000,
                LocalDate.of(2009, 11, 14));

        ludzie[1] = new Student(new String[]{"Wojciech"}, "Blala",
                LocalDate.of(1991, 9, 30), true, "Informatyka", 3.2);
//        ludzie[2] = new Osoba(new String[]{"Maria", "Wuria", "Buria"}, "Dipol",
//                LocalDate.of(1992,1,2));

        for (Osoba p : ludzie) {
            System.out.println(p.getImionaAsString() + " " +
                    p.getNazwisko() + ": " + p.getDataUrodzenia() + " " + p.getOpis());
        }
    }
}