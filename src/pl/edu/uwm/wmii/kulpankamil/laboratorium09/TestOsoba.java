package pl.edu.uwm.wmii.kulpankamil.laboratorium09;

import java.time.LocalDate;

public class TestOsoba {
    public static void main(String[] args) {
        Osoba grupa[] = new Osoba[5];

        grupa[0] = new Osoba("AB", LocalDate.of(1998, 3, 3));
        grupa[1] = new Osoba("AA", LocalDate.of(1998, 4, 3));
        grupa[2] = new Osoba("BB", LocalDate.of(1999, 5, 3));
        grupa[3] = new Osoba("CC", LocalDate.of(1999, 11, 29));
        grupa[4] = new Osoba("Kulpan", LocalDate.of(1998, 5, 30));
        System.out.println(grupa[0].compareTo(grupa[1]));

        for (Osoba o : grupa) {
            o.wypisz();
            System.out.println(o.toString());
        }
    }
}
