package pl.edu.uwm.wmii.kulpankamil.laboratorium09;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Osoba implements Comparable<Osoba> {

    private String nazwisko;
    private LocalDate dataUrodzenia;

    Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    @Override
    public int compareTo(Osoba o) {
        if (this.nazwisko.compareTo(o.nazwisko) == 0)
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        return this.nazwisko.compareTo(o.nazwisko);
    }

    @Override
    public boolean equals(Object obj) {
        if (this.getClass() == obj.getClass()) {
            Osoba other = (Osoba) obj;
            return this.nazwisko.equals(other.nazwisko) &&
                    this.dataUrodzenia == other.dataUrodzenia;
        }
        return false;
    }

    public int ileLat() {
        LocalDate output = LocalDate.now().minusYears(this.dataUrodzenia.getYear());
        return output.getYear();
    }

    public int ileMiesiecy() {
        LocalDate output = LocalDate.now().minusMonths(this.dataUrodzenia.getMonthValue());
        return output.getMonthValue();
    }

    public int ileDni() {
        LocalDate output = LocalDate.now().minusDays(this.dataUrodzenia.getDayOfMonth());
        return output.getDayOfMonth();
    }

    public void wypisz() {
        System.out.println("Lat: " + ileLat() +
                ", Miesiecy: " + ileMiesiecy() + ", Dni: " + ileDni());
    }

    @Override
    public String toString() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return getClass().getSimpleName() + "[" + this.nazwisko +
                " " + this.dataUrodzenia.format(dateTimeFormatter) + "]";

//        return getClass().getSimpleName() + "[" +
//                this.nazwisko + " " + this.dataUrodzenia.getYear() + "-" +
//                this.dataUrodzenia.getMonthValue() + "-" +
//                this.dataUrodzenia.getDayOfMonth() + "]";
    }
}
