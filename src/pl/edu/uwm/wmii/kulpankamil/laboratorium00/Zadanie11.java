package pl.edu.uwm.wmii.kulpankamil.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println("Najdziesz tu fraszkę dobrą, najdziesz złą i śrzednią,\n" +
                "\n" +
                "Nie wszytkoć mury wiodą materyją przednią;\n" +
                "\n" +
                "Z boków cegłę rumieńszą i kamień ciosany,\n" +
                "\n" +
                "W pośrzodek sztuki kładą i gruz brakowany.");
    }
}
