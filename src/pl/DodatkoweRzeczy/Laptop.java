package pl.DodatkoweRzeczy;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer> {

    private boolean czyApple;

    Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple) {
        super(nazwa, dataProdukcji);
        this.czyApple = czyApple;
    }

    boolean getCzyApple() {
        return czyApple;
    }

    @Override
    public int compareTo(Komputer o) {
        int tmp = super.compareTo(o);
        if (tmp == 0) {
            if (o instanceof Laptop) {
                Laptop otherobj = (Laptop) o;
                return Boolean.compare(otherobj.getCzyApple(), czyApple);
            }
        }
        return tmp;
    }

//    public Laptop clone() {
//        return (Laptop) super.clone();
//    }

    @Override
    public Object clone() {
        return super.clone();
    }


    @Override
    public String toString() {
        return super.toString() + " CzyApple? - " + czyApple;
    }
}
