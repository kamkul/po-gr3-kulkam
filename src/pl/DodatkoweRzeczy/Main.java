package pl.DodatkoweRzeczy;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Komputer> grupa = new ArrayList<>();
        grupa.add(new Komputer("1", LocalDate.of(1993, 1, 2)));
        grupa.add(new Komputer("1", LocalDate.of(1992, 3, 2)));
        grupa.add(new Komputer("2", LocalDate.of(1993, 1, 2)));
        grupa.add(new Komputer("1", LocalDate.of(1994, 1, 2)));
        grupa.add(new Komputer("2", LocalDate.of(1992, 1, 2)));

        ArrayList<Laptop> grupaLaptopow = new ArrayList<>();
        grupaLaptopow.add(new Laptop("1", LocalDate.of(1993, 1, 2), true));
        grupaLaptopow.add(new Laptop("1", LocalDate.of(1992, 3, 2), false));
        grupaLaptopow.add(new Laptop("2", LocalDate.of(1993, 1, 2), true));
        grupaLaptopow.add(new Laptop("1", LocalDate.of(1994, 1, 2), false));
        grupaLaptopow.add(new Laptop("2", LocalDate.of(1992, 1, 2), true));

        for (Komputer komputer : grupa)
            System.out.println(komputer.toString());
        grupaLaptopow.sort(Laptop::compareTo);

        System.out.println("------------------");
        for (Komputer komputer : grupa)
            System.out.println(komputer.toString());
        System.out.println("\n------------------\n");

        for (Laptop laptop : grupaLaptopow)
            System.out.println(laptop.toString());

        grupa.sort(Komputer::compareTo);
        System.out.println("------------------");
        for (Laptop laptop : grupaLaptopow)
            System.out.println(laptop.toString());
    }
}
