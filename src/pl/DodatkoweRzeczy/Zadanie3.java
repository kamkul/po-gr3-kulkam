package pl.DodatkoweRzeczy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Zadanie3 {
    public static void main(String[] args) throws IOException {
        for (String fileName : args) {
            Path path = Paths.get(fileName);

            byte[] bytes = Files.readAllBytes(path);
            int sep = 0;
            for (byte elem : bytes) {
                if (sep == 64){
                    System.out.println();
                    sep = 0;
                }
                if (elem > 32 && elem < 126) {
                    System.out.print(Character.valueOf((char) elem));
                    sep++;
                }
            }
            System.out.println("\n");
        }
    }
}
