package pl.DodatkoweRzeczy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

public class Zadanie2 {

    public static <T extends Iterable<E>, E> void print(T objects) {
        StringBuilder sb = new StringBuilder();
        for (E element : objects)
            sb.append(element).append(", ");
        sb.delete(sb.length() - 2, sb.length() - 1);
        System.out.println(sb);
    }

    public static void main(String[] args) {
        ArrayList<String> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();

        LinkedList<String> c = new LinkedList<>();
        LinkedList<Integer> d = new LinkedList<>();

        HashSet<String> e = new HashSet<>();
        HashSet<Integer> f = new HashSet<>();

        a.add("AA");
        a.add("BB");
        b.add(1);
        b.add(2);

        c.add("CC");
        c.add("DD");
        d.add(3);
        d.add(4);

        e.add("EE");
        e.add("FF");
        f.add(5);
        f.add(6);

        print(a);
        print(b);
        print(c);
        print(d);
        print(e);
        print(f);
    }
}
