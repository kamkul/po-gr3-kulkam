package pl.DodatkoweRzeczy;

public class ArrayUtil {

    public static <T extends Comparable<? super T>> boolean isSorted(T array[]) {
        for (int i = 0; i < array.length - 1; i++)
            if (array[i].compareTo(array[i + 1]) > 0)
                return false;
        return true;
    }

    public static void main(String[] args) {
        Integer[] a = {1,2,3,4,5,6};
        Integer[] b = {1,1,1,1,1,3};
        Integer[] c = {3,1,1,1,1,3};
        System.out.println(isSorted(a));
        System.out.println(isSorted(b));
        System.out.println(isSorted(c));
    }
}

