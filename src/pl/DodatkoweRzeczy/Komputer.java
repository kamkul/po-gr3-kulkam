package pl.DodatkoweRzeczy;

import java.time.LocalDate;

public class Komputer implements Cloneable, Comparable<Komputer> {

    private String nazwa;
    private LocalDate dataProdukcji;

    Komputer(String nazwa, LocalDate dataProdukcji) {
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }

    public LocalDate getDataProdukcji() {
        return dataProdukcji;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setDataProdukcji(LocalDate dataProdukcji) {
        this.dataProdukcji = dataProdukcji;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

//    @Override
//    public int compareTo(Komputer o) {
//        int tmp = o.getNazwa().compareTo(nazwa);
//        return (tmp == 0)
//                ? dataProdukcji.compareTo(o.getDataProdukcji())
//                : tmp;
//    }


    @Override
    public int compareTo(Komputer o) {
        int tmp = nazwa.compareTo(o.nazwa);
        if (tmp == 0)
            return dataProdukcji.compareTo(o.dataProdukcji);
        return tmp;
    }

    /**
     * Wzorowałem się na clone() z ArrayList.
     * <p>
     * Jest to w ten sposób napisane
     * żeby nie trzeba było dodawać sygnatury throws CloneNotSupportedException do
     * maina
     *
     * @return sklonowany obiekt
     */

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e);
        }
    }

//    public Komputer clone() {
//        try {
//            return (Komputer) super.clone();
//        } catch (CloneNotSupportedException e) {
//            throw new InternalError(e);
//        }
//    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Komputer) {
            Komputer otherobj = (Komputer) obj;
            return otherobj.getNazwa().equals(nazwa) &&
                    otherobj.getDataProdukcji().equals(dataProdukcji);
        }
        return false;
    }

    @Override
    public String toString() {
        return nazwa + " " + dataProdukcji.toString();
    }
}
