package pl.imiajd.kulpan;

public class Osoba {
    private String nazwisko;
    private String rok_urodzenia;

    Osoba(String imie, String nazwisko){
        this.nazwisko = nazwisko;
        this.rok_urodzenia = imie;
    }

    public String getRok_urodzenia() {
        return rok_urodzenia;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    @Override
    public String toString() {
        return "Nazwisko to :" +
                nazwisko +
                "\tRok urodzenia to: " +
                rok_urodzenia;
    }
}
