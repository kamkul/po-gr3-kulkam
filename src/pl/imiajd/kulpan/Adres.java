package pl.imiajd.kulpan;

import java.text.DecimalFormat;

public class Adres {
    private String ulica;
    private String numer_domu;
    private String numer_mieszkania;
    private String miasto;
    private int kod_pocztowy;

    Adres(String ulica, String numer_domu, String numer_mieszkania, String miasto, int kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    Adres(String ulica, String numer_domu, String miasto, int kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = "Nie podano/Nie dotyczy";
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    void pokaz() {
        DecimalFormat decimalFormat = new DecimalFormat("00,000");
        String kod_pocztowy = decimalFormat.format(this.kod_pocztowy);
        kod_pocztowy = kod_pocztowy.replace(kod_pocztowy.charAt(2), '-');
        System.out.println(kod_pocztowy + " " + miasto + "\n"
                + ulica + " " + numer_domu + " " + numer_mieszkania);
    }

    public int getKod_pocztowy() {
        return kod_pocztowy;
    }

    public boolean przed(Adres adres) {
        return kod_pocztowy < adres.getKod_pocztowy();
    }

}
