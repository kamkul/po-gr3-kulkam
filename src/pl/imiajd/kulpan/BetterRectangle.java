package pl.imiajd.kulpan;

import java.awt.*;

public class BetterRectangle extends Rectangle {

//    BetterRectangle(int x, int y, int width, int height){
//        this.setLocation(x,y);
//        this.setSize(width, height);
//    }

    BetterRectangle(int x, int y, int width, int height){
        super(x,y,width,height);
    }

    public double getPerimeter() {
        return 2 * width + 2 * height;
    }

    public double getArea() {
        return width * height;
    }
}
