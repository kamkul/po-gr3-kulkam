package pl.imiajd.kulpan;

public class Student extends Osoba {
    private String kierunek;

    Student(String nazwisko, String rok_urodzenia, String kierunek) {
        super(nazwisko, rok_urodzenia);
        this.kierunek = kierunek;
    }

    public String getKierunek() {
        return kierunek;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\tKierunek to:" +
                kierunek;
    }
}
