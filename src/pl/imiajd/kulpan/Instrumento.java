package pl.imiajd.kulpan;

import java.time.LocalDate;

public abstract class Instrumento {

    String producent;
    LocalDate rokProdukcji;

    Instrumento(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }

    abstract void dzwiek();

    @Override
    public String toString() {
        return "Producent: " +
                producent +
                "\tRok produkcji + " +
                rokProdukcji;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Instrumento))
            return false;
        return rokProdukcji.equals(((Instrumento) obj).rokProdukcji) &&
                producent.equals(((Instrumento) obj).producent);
    }
}
