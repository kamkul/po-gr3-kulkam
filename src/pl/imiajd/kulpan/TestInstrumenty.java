package pl.imiajd.kulpan;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {

    public static void main(String[] args) {
        ArrayList<Instrumento> arrayList = new ArrayList<>();
        Skrzypce skrzypce2 = new Skrzypce("LALALA", LocalDate.of(1992,10,30));
        Flet flet1 = new Flet("LALALA", LocalDate.of(1992,10,30));
        Flet flet2 = new Flet("LALALA", LocalDate.of(1992,10,30));
        Fortepian fortepian = new Fortepian("LALALA", LocalDate.of(1992,10,30));

        arrayList.add(skrzypce2);
        arrayList.add(flet1);
        arrayList.add(flet2);
        arrayList.add(fortepian);

        for(Instrumento element : arrayList){
            element.dzwiek();
        }
    }

}
