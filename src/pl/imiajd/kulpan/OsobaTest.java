package pl.imiajd.kulpan;

public class OsobaTest {
    public static void main(String[] args) {
        Nauczyciel nauczyciel = new Nauczyciel( "A", "1980", 3000);
        Osoba osoba = new Osoba( "B", "1984");
        Student student = new Student( "C", "1986", "BLa");

        System.out.println(nauczyciel.toString());
        System.out.println(student.toString());
        System.out.println(osoba.toString());
    }
}
