package pl.imiajd.kulpan;

public class Nauczyciel extends Osoba {
    private double pensja;

    Nauczyciel(String nazwisko, String rok_urodzenia, double pensja) {
        super(nazwisko, rok_urodzenia);
        this.pensja = pensja;
    }

    public double getPensja() {
        return pensja;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\tPensja to: " +
                pensja;
    }
}
