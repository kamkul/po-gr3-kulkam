package pl.imiajd.kulpan;

import java.time.LocalDate;

public class Skrzypce extends Instrumento {

    Skrzypce(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    void dzwiek() {
        System.out.println("LULULU");
    }
}
