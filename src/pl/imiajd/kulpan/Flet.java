package pl.imiajd.kulpan;

import java.time.LocalDate;

public class Flet extends Instrumento {

    Flet(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    void dzwiek() {
        System.out.println("FUFUFUFU");
    }
}
